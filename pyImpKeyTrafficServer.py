
# -*- coding: utf-8 -*-


import logging
import logging.config

import os
import shutil
import json
import glob
import time
from datetime import datetime
from datetime import timedelta

import ECE.GetRestJson as GetRestJson
import ECE.Put_Content_In_ECE as Put_Content_In_ECE
import ECE.Prendi_Content_Da_ECE as Prendi_Content_Da_ECE
import ECE.pyLockContent as pyLockContent
import Resources.pySettaVars as pySettaVars
import DBApi.API_DB as API_DB
import COUCHDB.pyCouchDbDrive as pyCouchDbDrive
import SOLR.pySolrServices as pySolrServices


def SettaEnvironment( debug ):

        if (debug):
                print 'PRIMA DI SETTARE ENVIRONMENT'
        if (debug):
                print 'ENV : '
        if (debug):
                print os.environ

        if (debug):
                print '---------------------------------'

        Environment_Variables = {}

        print 'verifico il setting della variabile _ImportKeyTrafficServerEnv_ '
        if '_ImportKeyTrafficServerEnv_' in os.environ:
                if 'PRODUCTION' in os.environ['_ImportKeyTrafficServerEnv_']:
                        print 'variabile _ImportKeyTrafficServerEnv_ ha valore \'PRODUCTION\''
                        print 'setto ENV di PROD'
                        Environment_Variables = pySettaVars.PROD_Environment_Variables
                        pySettaVars.LOGS_CONFIG_FILE = pySettaVars.PROD_CONFIG_FILE
                        pySettaVars.LOGGING_FILE = pySettaVars.PROD_LOGGING_FILE
                else:
                        print 'variabile _ImportKeyTrafficServerEnv_ ha valore : ' + os.environ['_ImportKeyTrafficServerEnv_']
                        print 'diverso da \'PRODUCTION\''
                        print 'setto ENV di TEST'
                        Environment_Variables = pySettaVars.TEST_Environment_Variables
                        pySettaVars.LOGS_CONFIG_FILE = pySettaVars.STAG_CONFIG_FILE
                        pySettaVars.LOGGING_FILE = pySettaVars.STAG_LOGGING_FILE

        else:
                print 'variabile _ImportKeyTrafficServerEnv_ non trovata: setto ENV di TEST'
                Environment_Variables = pySettaVars.TEST_Environment_Variables
		pySettaVars.LOGS_CONFIG_FILE = pySettaVars.STAG_CONFIG_FILE
		pySettaVars.LOGGING_FILE = pySettaVars.STAG_LOGGING_FILE


        for param in Environment_Variables.keys():
                #print "%20s %s" % (param,dict_env[param])
                os.environ[param] = Environment_Variables[ param ]


        if (debug):
                print 'DOPO AVER SETTATO ENVIRONMENT'
        if (debug):
                print 'ENV : '
        if (debug):
                print os.environ



def Prendi_Lista( workingDir ):

	result = {}
	
	lista_files = glob.glob( workingDir + '*.jpg' )
	lista_files = lista_files + glob.glob( workingDir + '*.JPG' )
	
	# dove trovo filenames del tipo : GP765997_P00000011_1.jpg
	# con ancora tutto il path
	# e a me interessano il primo campo = LegacyId
	# e il numero prima del . che rappresenta quanti ce ne sono per quel LegacyId

	for lis in lista_files:
		nome_file = os.path.basename(lis)
		print nome_file
		componenti = nome_file.split('_')
		if len(componenti) > 2:
			legacyId = componenti[0]
			progressivo = componenti[-1]
			if legacyId in result:
				result[legacyId].append( lis )
			else:
				result[legacyId] = [ lis ]
			
	# qui restituisco un json tipo :
	# {'GP765997': ['/home/perucccl/Webservices/STAGING/ImportKeyFramesTrafficServer/Ftp_Dir/GP765997_P00000011_3.jpg', '/home/perucccl/Webservices/STAGING/ImportKeyFramesTrafficServer/Ftp_Dir/GP765997_P00000011_2.jpg'], 'GP765666': ['/home/perucccl/Webservices/STAGING/ImportKeyFramesTrafficServer/Ftp_Dir/GP765666_P00000011_2.jpg', '/home/perucccl/Webservices/STAGING/ImportKeyFramesTrafficServer/Ftp_Dir/GP765666_P00000011_1.jpg']}
	# nota il 3 che mi dice che probabilmente ne ho gia' importati 2 per quel legacy id

	return result



def Importa_Img( lista_immagini ):

	logger.debug('------------------------ INIT Importa_Img -------------- ')

	result = {}
	lista_rimaste = {}
	lista_archivio = []

	section = os.environ['IMPORT_SECTION']
	
	# dove trovo filenames del tipo : GP765997_P00000011_1.jpg
	# con ancora tutto il path
	# e a me interessano il primo campo = LegacyId
	# e il numero prima del . che rappresenta quanti ce ne sono per quel LegacyId

	for legacy, img in lista_immagini.iteritems():
		# in img ho la lista di file_path che passo per upload del binary
		for file_path in img:
			logger.debug( ' file path = ' + file_path )
		
			binary = Put_Content_In_ECE.UploadBinary( file_path )
			if not binary[0] :
				# non son riuscito a fare la load del file ....
				# continuo senza cambiare img con id
				if legacy in lista_rimaste:
					lista_rimaste[legacy].append(file_path)
				else:
					lista_rimaste[legacy] = [file_path]
				continue
				
			else:

				# ho fatto upload binary e in binary[1] ho la location url del binary
				# da passare alla creazione del content picture
				result_crea = Put_Content_In_ECE.Create_Img( binary[1] , legacy, section )
				
				if not result_crea[0]:
					# con quel binary non sono riuscito a costruire un asset immagine
					if legacy in lista_rimaste:
						lista_rimaste[legacy].append(file_path)
					else:
						lista_rimaste[legacy] = [file_path]
					continue
				else:
					# ho creato img ed e andato tutto bene
					if legacy in result:
						result[legacy].append(result_crea[1])
					else:
						result[legacy] = [ result_crea[1] ]
				
					# e andato tutto bene posso spostarla nella directory di archiviazione
					# e la muovo in archived
					logger.debug( 'passo da sposta Img' )
					Sposta_Img( file_path )

	logger.debug( ' result = ' + str(result))
	logger.debug( ' rimaste = ' + str(lista_rimaste))
	logger.debug('------------------------ END Importa_Img -------------- ')

	return [ result , lista_rimaste ]

def Merge_Liste( lista_nuova, lista_old ):

	logger.debug('------------------------ INIT Merge_Liste -------------- ')
	result = {}
	

	try:
			
		result = lista_nuova.copy()
		
		for key, valueList in result.iteritems():
			if key in lista_old:
				# in lista_old[ key ] ho una lista di ECE ID 
				# da aggiungere alla result[ key ] 
				for lis in lista_old[ key ]:
					result[ key ].append( lis )
		for key, valueList in lista_old.iteritems():
			if key not in result:
				result[ key ] = valueList
				
	except Exception as e:
		logger.debug( 'PROBLEMI in Merge_Liste : ' + str(e) )
		return {}


	logger.debug( ' result = ' + str(result))
	logger.debug('------------------------ finisce Merge_Liste -------------- ')
        return result

def Sposta_Img(  file_path ):
	
	logger.debug('------------------------ INIT Sposta_Img -------------- ')
        try:
		file_name = os.path.basename( file_path )
		logger.debug( "mv " + file_path + " to -> " + os.environ['FTP_ARCHIVE_DIR'] + file_name )
		shutil.move( file_path,  os.environ['FTP_ARCHIVE_DIR'] + file_name )
		
	except Exception as e:
		logger.error('ERROR: EXCEPT in Sposta_Img  = ' + str(e))
		logger.error('ERROR: EXCEPT in Sposta_Img per immagine = ' + file_path)
		pass

	logger.debug('------------------------ END Sposta_Img -------------- ')

def checkImportKeyframes( listaTot, couchDb, taip ):
	result = True

	for lis in listaTot:
		# in lis ho la lista degli id
		# devo prenderne il contenuto
		[ res, treeRoot, xml ] = Prendi_Content_Da_ECE.getIdDouble( lis )
		#print treeRoot
		if not res :
			logger.warning(' in checkImportKeyframes problema getIdDouble con id : ' + lis )
			continue

		if 'mamProgrammeVideo' in taip:
			# devo prendere il suo programme 
			[ res, programmeId ] = Prendi_Content_Da_ECE.PrendiValFromXml( treeRoot, 'programmeId' )
			if not res :
				logger.warning(' in checkImportKeyframes problema PrendiValFromXml con programmeId : ' + lis )
				continue
		else:
			programmeId = lis

		print programmeId
		# devo prenderne il contenuto
		[ res, programmeTreeRoot, programmeXml ] = Prendi_Content_Da_ECE.getIdDouble( programmeId )
		#print treeRoot
		if not res :
			logger.warning(' in checkImportKeyframes problema getIdDouble del programme id : ' + str(programmeId) + ' partendo da id : ' + str(lis ))
			continue

		# verificare il valore del campo episode_legacyid che arrivera nella forma rsi:mp:louise:GP945144
		[ res, legacyId ] = Prendi_Content_Da_ECE.PrendiValFromXml( programmeTreeRoot, 'episode_legacyid' )
		if not res :
			logger.warning(' in checkImportKeyframes problema PrendiValFromXml di episode_legacyid id : ' + programmeId  + ' partendo da id : ' + lis)
			continue

		legacyId =  legacyId.split(':')[-1]
		print legacyId
	
		# verificare usando la getDbDocId del Couch Driver e se il keyframe esiste oppure no
		couchValue =  couchDb.getDbDocId( legacyId ) 
		if 'error' in couchValue:
			logger.warning(' in checkImportKeyframes non esiste keyframe per id : ' + lis )
			continue
		# nel caso esista aggiungerlo alla relazione come editoriaKeyFrame
		# mi torna una roba cosi :
		# {u'_rev': u'3-295a646223cbc5346186747df7456546', u'_id': u'CPTHE6666', u'keyframes': [u'12687951']}
		if 'keyframes' in couchValue:
			key = couchValue['keyframes'][0]
			logger.debug( 'trovato keyframe : ' +  str(key))
		else:
			logger.warning(' in checkImportKeyframes non riesco a prendere keyframe : ' + couchValue['keyframes'][0]  )
			continue

		if 'mamProgrammeVideo' in taip:
			if not Put_Content_In_ECE.addEditorialKeyframe( lis, xml, key ):
				logger.warning(' in checkImportKeyframes problema con Put_Content_In_ECE.addEditorialKeyframe : ' + lis )
				continue
		else:
			if not Put_Content_In_ECE.addKeyframeToLead( programmeId, programmeXml, key ):
				logger.warning(' in checkImportKeyframes problema con Put_Content_In_ECE.addKeyframeToLead : ' + programmeId + ' partendo da id : ' + lis)
				continue

		

	return result


if __name__ == "__main__":

        print '-\n'
        print '------------ INIT ---------- pyImportKeyTrafficServer.py ---------------------'
        #print os.environ

        # questa e per settare le veriabili di ambiente altrimenti come cron non andiamo da nessuna parte
        SettaEnvironment( False )

        print '\n\n log configuration file : ' +  pySettaVars.LOGS_CONFIG_FILE + '\n'
        logging.config.fileConfig(pySettaVars.LOGS_CONFIG_FILE, defaults={'logfilename': pySettaVars.LOGGING_FILE})
        logger = logging.getLogger('pyImportKeyTrafficServer')
        print ' logging file : \t\t' + logger.handlers[0].baseFilename + '\n'


        logger.info( '------------ INIT ---------- pyImportKeyTrafficServer.py ---------------------')
	# calcolo le zulu per now e now - 5 minuti
	dateTo =  datetime.now()
	dateFrom =  datetime.now()-timedelta(seconds=300)
	#logger.debug(CLADEBUG -)
	# dateFrom =  datetime.now()-timedelta(seconds=15000)
	#logger.debug(CLADEBUG -)
	dateToStr =  dateTo.strftime('%Y-%m-%dT%H:%M:00Z')
	dateFromStr =  dateFrom.strftime('%Y-%m-%dT%H:%M:00Z')
	# le date sono nella forma : 2021-04-27T16:04:00Z
	#logger.debug(CLADEBUG -)
	#dateToStr =  '2021-05-18T13:25:00Z'
	#dateFromStr = '2021-05-18T13:20:00Z'
	#logger.debug(CLADEBUG -)
	
	# prendo i programme e programmeVideo che sono stati creati negli ultimi 5 minuti
	#listaTot = pySolrServices.solrPrendiContent('4', 'mamProgramme', dateFromStr,  dateToStr )

	for taip in [ 'mamProgrammeVideo', 'mamProgramme']:
		
		listaTot = pySolrServices.solrPrendiContent('4', taip, dateFromStr,  dateToStr)
		print listaTot
		#'CLADEBUG'
		#listaTot = ['13948990']
		#'CLADEBUG'
		if len(listaTot) < 1:
			logger.info('trovati 0 asset ' + taip + ' da controllare ESCO')
			print '------------ END ---------- pyImportKeyTrafficServer.py ---------------------'
			logger.info('------------ END ---------- pyImportKeyTrafficServer.py ---------------------')
			continue
		else:
			logger.info('trovati : ' + str(len(listaTot)) + ' asset ' + taip + ' da controllare PROCEDO')

		# # prendo i keyframes che sono stati inseriti nel CouchDb
		couchDb = pyCouchDbDrive.CouchDbDriver(os.environ['COUCH_DB'])

		if checkImportKeyframes( listaTot, couchDb , taip):
			logger.info('checkImportKeyframesPV tutto ok per : ' + taip)
		else:
			logger.warning('checkImportKeyframesPV trovato problema per :' + taip)
	
